/*
 *  DBF to SQLite conversion tool
 *  Copyright (C) 2011  Stuart Longland <me@vk4msl.yi.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef _DEBUG_H
#define _DEBUG_H

#include <stdio.h>

/* Debugging levels */
#define DBGLOG_CRITICAL	0
#define DBGLOG_ERROR	1
#define DBGLOG_WARNING	2
#define DBGLOG_INFO	3
#define DBGLOG_DETAIL	4
#define DBGLOG_DEBUG_H	5
#define DBGLOG_DEBUG_M	6
#define DBGLOG_DEBUG_L	7

/*! Debugging level: higher number means more detail */
static uint8_t debug_level = DBGLOG_INFO;

/*! Log file handle */
static FILE* debug_log = NULL;

/*! Write a message to the log */
#define debug_printf(lvl, args...) \
	if (debug_log && (lvl <= debug_level)) \
		fprintf(debug_log, args)

#endif
