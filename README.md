This implements a crude SQLite3 to DBase 3/4 conversion tool, based on the
specifications at:

http://www.clicketyclick.dk/databases/xbase/format/index.html

Three tools are included:
- `dbfrepack`: a DBF file repacker.  This is useful if you have
  OpenOffice-generated DBase 3 files with deleted records, as OpenOffice does
  not implement a repacker.
- `dbftosqlite`: DBF to SQLite3 conversion tool, reads the given DBase 3/4
  files and stores them in SQLite3 tables.
- `sqlitetodbf`: Does the reverse of `dbftosqlite`.

The tool creates a table; `fieldspec`, which is a representation of the DBase
III/IV field header data.  One table is created then, per DBF file; the name is
the basename of the file minus the .dbf extension, and prefixed with `dbf_`.

A second table, `tablespec` defines properties that are set per table such as
reserved field data, the table version, database container, MEMO file version,
etc.

MEMO fields are supported, Indexes are not (yet).
