/*
 *  DBF to SQLite conversion tool
 *  Copyright (C) 2011  Stuart Longland <me@vk4msl.yi.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <assert.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#include <sqlite3.h>

#include "dbf.h"
#include "debug.h"

void writeText(sqlite3_stmt* dbs, int field_id,
		struct dbf_field_desc* field, FILE* dbf) {
	const unsigned char* f = sqlite3_column_text(dbs, field_id);
	char* buffer = malloc(field->length);
	assert(buffer != NULL);
	memset(buffer, ' ', field->length);
	debug_printf(DBGLOG_DEBUG_L, "Char Field    %s = \"%s\".\n",
			field->name, f);
	if (f) {
		size_t l = strlen((const char*)f);
		if (field->length < l)
			l = field->length;

		memcpy(buffer, f, l);
	}
	fwrite(buffer, sizeof(char), field->length, dbf);
	free(buffer);
}

void writeNumber(sqlite3_stmt* dbs, int field_id,
		struct dbf_field_desc* field, FILE* dbf) {
	char* buffer = malloc(field->length+2);
	assert(buffer != NULL);
	memset(buffer, ' ', field->length+2);
	if (field->count) {
		char format[8];
		double fd = sqlite3_column_double(dbs, field_id);
		snprintf(format, sizeof(format), "%%%d.%df",
				field->length,
				field->count);
		snprintf(buffer, field->length+1, format, fd);
		debug_printf(DBGLOG_DEBUG_L, "Numeric Field %s = \"%s\".\n",
				field->name, buffer);
		buffer[strlen(buffer)] = ' ';
		fwrite(buffer, sizeof(char), field->length, dbf);
	} else {
		char format[8];
		int fi = sqlite3_column_int(dbs, field_id);
		snprintf(format, sizeof(format), "%%%dd",
				field->length);
		snprintf(buffer, field->length+1, format, fi);
		debug_printf(DBGLOG_DEBUG_L, "Numeric Field %s = \"%s\".\n",
				field->name, buffer);
		buffer[strlen(buffer)] = ' ';
		fwrite(buffer, sizeof(char), field->length, dbf);
	}
	free(buffer);
}

void writeDate(sqlite3_stmt* dbs, int field_id,
		struct dbf_field_desc* field, FILE* dbf) {
	/* Format assumed from SQLite is YYYY-MM-DD */
	char buffer[8];
	const unsigned char* f = sqlite3_column_text(dbs, field_id);
	debug_printf(DBGLOG_DEBUG_L, "Date Field    %s = \"%s\".\n",
			field->name, f);
	if (f && (strlen((const char*)f) >= 10)) {
		buffer[0]	= f[0];
		buffer[1]	= f[1];
		buffer[2]	= f[2];
		buffer[3]	= f[3];
		buffer[4]	= f[5];
		buffer[5]	= f[6];
		buffer[6]	= f[8];
		buffer[7]	= f[9];
	}
	fwrite(buffer, sizeof(char), sizeof(buffer), dbf);	
}

void writeMemo(sqlite3_stmt* dbs, int field_id,
		struct dbf_field_desc* field, FILE* dbf,
		FILE* dbt, uint8_t dbt_version, size_t* dbt_usage) {
	const unsigned char* f = sqlite3_column_text(dbs, field_id);
	size_t fl = 0;
	char* buffer = malloc(field->length + 1);
	assert(buffer != NULL);
	memset(buffer, ' ', field->length);
	buffer[field->length] = 0;
	debug_printf(DBGLOG_DEBUG_L, "Memo Field    %s = \"%s\".\n",
			field->name, f);

	if (f)
		fl = strlen((const char*)f);
	if (dbt && fl) {
		/* Block structure */
		struct dbt_usedblock_header block;

		/* Block number used */
		size_t block_id = *dbt_usage;

		debug_printf(DBGLOG_DEBUG_L,
				"Raw field length: %lu bytes\n"
				"DBT version %d\n"
				"Block ID is %lu\n", (unsigned long)fl,
				dbt_version, (unsigned long)block_id);

		/* Count the terminator we'll add at the end */
		fl += 2;
		debug_printf(DBGLOG_DEBUG_L,
				"Field length with header: %lu bytes\n",
				(unsigned long)fl);

		/* Count the number of newlines; these will double in size */
		{
			const unsigned char* p = f;
			while(*p) {
				if (*p == '\n')
					fl++;
				p++;
			}
		}

		debug_printf(DBGLOG_DEBUG_L,
				"Field length with newlines: %lu bytes\n", 
				(unsigned long)fl);
		memset(&block, 0, sizeof(block));

		/* Write out block header */
		if (dbt_version != DBT_VER3) {
			block.magic = DBT_USED_MAGIC;
			block.length = sizeof(block)+fl;
			dbt_usedblock_header_to_disk(&block);
			debug_printf(DBGLOG_DEBUG_L,
					"Writing out block header.\n");
			fwrite(&block, sizeof(block), 1, dbt);
			dbt_usedblock_header_to_host(&block);
		}

		/* Write out memo data */
		const unsigned char* in = f;
		char block_data[DBT_BS];
		while (*in) {
			char block_data[DBT_BS];
			char* out = block_data;
			size_t block_len = DBT_BS;

			/* Fill up a block */
			while (*in && block_len) {
				if (*in == '\n') {
					/* Translate \n to 0x8d 0x0a */
					if (block_len < 2)
						break;
					out[0] = DBT_LE0;
					out[1] = DBT_LE1;
					out++;
					block_len--;
				} else
					*out = *in;
				out++;
				in++;
				block_len--;
			}

			/* Write it out */
			debug_printf(DBGLOG_DEBUG_L,
					"Writing out block.\n");
			fwrite(block_data, 1, DBT_BS - block_len, dbt);
		}

		if (dbt_version != DBT_VER3) {
			/* Terminate the memo field */
			block_data[0] = DBT_BE0;
			block_data[1] = DBT_BE1;
			debug_printf(DBGLOG_DEBUG_L,
					"Writing out terminator.\n");
			fwrite(block_data, 1, 2, dbt);
		}

		if (block.length % DBT_BS) {
			/* We have still some block space left, pad with field terminators */
			char block_data[DBT_BS];
			memset(block_data, DBT_EOB, sizeof(block_data));
			block_data[0] = 0;
			fwrite(block_data, 1, DBT_BS - (block.length % DBT_BS), dbt);
			debug_printf(DBGLOG_DEBUG_L,
					"Writing out padding.\n");
		}

		/* Update the number of blocks used */
		*dbt_usage += (block.length / DBT_BS) + 1;
		debug_printf(DBGLOG_DEBUG_L,
				"Next free block ID %lu.\n",
				(unsigned long)(*dbt_usage));

		/* Write out block ID */
		snprintf(buffer, field->length+1, "%10lu",
				(unsigned long)block_id);
		fwrite(buffer, sizeof(char), field->length, dbf);
		debug_printf(DBGLOG_DEBUG_L,
				"Memo Field    %s = \"%s\" (in dbf).\n",
				field->name, buffer);
	} else {
		/* Special case; no MEMO */
		fwrite(buffer, sizeof(char), field->length, dbf);
		debug_printf(DBGLOG_DEBUG_L,
				"Memo Field    %s = \"%s\" (in dbf).\n",
				field->name, buffer);
	}
	free(buffer);
}

int writeTable(sqlite3* db, const char* dir, const char* table) {
	char sql[1024];
	char buffer[1024];
	uint8_t dbc[DBF_DBC_LEN];
	uint8_t* header_res2 = NULL;
	size_t header_res2_sz = 0;
	uint8_t enable_dbc = 0;
	uint8_t enable_memo = 0;
	uint8_t got_tablespec = 0;
	size_t dbt_usage = 1;
	struct dbt_header dbth;

	int ordered_table = 0;

	/*
	 * Length:	{DIR} / {TABLE} .ext	\0 ---- 1
	 * 		|   | | |     | '--'-----------	4
	 * 		|   | | '-----'---------------- strlen(argv[2])
	 * 		|   | '------------------------ 1
	 *		'---'-------------------------- strlen(dir)
	 */
	char* dbf_name = calloc(strlen(dir) + strlen(table) + 6, sizeof(char));
	char* dbt_name = calloc(strlen(dir) + strlen(table) + 6, sizeof(char));
	strcpy(dbf_name, dir);
	strcat(dbf_name, "/");
	strcat(dbf_name, table);
	strcpy(dbt_name, dbf_name);
	strcat(dbf_name, ".dbf");
	strcat(dbt_name, ".dbt");

	debug_printf(DBGLOG_INFO,"Exporting table: %s\n", table);
	debug_printf(DBGLOG_DETAIL,"DBF File: %s\n", dbf_name);
	debug_printf(DBGLOG_DETAIL,"DBT File: %s\n", dbt_name);

	FILE* dbf = fopen(dbf_name, "wb+");
	FILE* dbt = NULL;
	if (!dbf) {
		debug_printf(DBGLOG_ERROR,"Could not open dbf file: %s", strerror(errno));
		free(dbf_name);
		free(dbt_name);
		return errno;
	}

	struct dbf_field_desc*	field = NULL;
	size_t			field_count = 0;
	struct dbf_table_header header;
	memset(&dbth, 0, sizeof(dbth));
	memset(&header, 0, sizeof(header));

	/* Update last updated timestamp */
	struct timeval tv;
	struct tm tm;
	gettimeofday(&tv, NULL);
	localtime_r(&tv.tv_sec, &tm);

	/* Generate a new header */
	header.last_update.year		= tm.tm_year;
	header.last_update.month	= tm.tm_mon + 1;
	header.last_update.day		= tm.tm_mday;

	sqlite3_stmt* dbs;
	sqlite3_snprintf(sizeof(sql), sql,
			"SELECT"
			"  dbf_version,"
			"  dbf_reserved1,"
			"  dbf_incomplete,"
			"  dbf_encryption,"
			"  dbf_free_record,"
			"  dbf_multiuser,"
			"  dbf_mdx,"
			"  dbf_language,"
			"  dbf_reserved2,"
			"  dbf_dbc,"
			"  dbf_reserved3,"
			"  dbt_version,"
			"  dbt_reserved1,"
			"  dbt_reserved2"
			" FROM tablespec"
			" WHERE tablename=%Q;",
			table);
	int rv = sqlite3_prepare(db, sql, -1, &dbs, NULL);
	if (rv == SQLITE_OK) {
		sqlite3_reset(dbs);
		rv = sqlite3_step(dbs);
		if (rv == SQLITE_ROW) {
			/* Okay, we've got our table specification */
			size_t blob_sz;
			got_tablespec = 1;

			header.version = sqlite3_column_int(dbs, 0);
			header.reserved1 = sqlite3_column_int(dbs, 1);
			header.incomplete = sqlite3_column_int(dbs, 2);
			header.encryption = sqlite3_column_int(dbs, 3);
			header.free_record = sqlite3_column_int(dbs, 4);

			if (sqlite3_column_blob(dbs, 5)) {
				blob_sz = sqlite3_column_bytes(dbs, 5);
				memcpy(header.multiuser,
					sqlite3_column_blob(dbs, 5),
					(blob_sz > sizeof(header.multiuser))
					? sizeof(header.multiuser) : blob_sz);
			}

			header.mdx = sqlite3_column_int(dbs, 6);
			header.language = sqlite3_column_int(dbs, 7);
			header.reserved2 = sqlite3_column_int(dbs, 8);

			if (sqlite3_column_blob(dbs, 9)) {
				blob_sz = sqlite3_column_bytes(dbs, 9);
				memset(dbc, 0, sizeof(dbc));
				memcpy(dbc,
					sqlite3_column_blob(dbs, 9),
					(blob_sz > sizeof(dbc))
					? sizeof(dbc) : blob_sz);
				enable_dbc = 1;
			}

			if (sqlite3_column_blob(dbs, 10)) {
				blob_sz = sqlite3_column_bytes(dbs, 9);
				header_res2 = malloc(blob_sz);
				if (header_res2) {
					header_res2_sz = blob_sz;
					memcpy(header_res2,
						sqlite3_column_blob(dbs, 10),
						blob_sz);
				} else {
					debug_printf(DBGLOG_ERROR,"failed to allocate %lu bytes "
						"for header reserved data.\n",
						(unsigned long)blob_sz);
					free(dbf_name);
					free(dbt_name);
					fclose(dbf);
					return -1;
				}
			}

			dbth.version = sqlite3_column_int(dbs, 11);

			if (sqlite3_column_blob(dbs, 12)) {
				blob_sz = sqlite3_column_bytes(dbs, 12);
				memcpy(dbth.reserved1,
					sqlite3_column_blob(dbs, 12),
					(blob_sz > sizeof(dbth.reserved1))
					? sizeof(dbth.reserved1) : blob_sz);
			}

			if (sqlite3_column_blob(dbs, 13)) {
				blob_sz = sqlite3_column_bytes(dbs, 13);
				memcpy(dbth.reserved2,
					sqlite3_column_blob(dbs, 13),
					(blob_sz > sizeof(dbth.reserved2))
					? sizeof(dbth.reserved2) : blob_sz);
			}
		}
	}

	/* Calculate record count */
	sqlite3_snprintf(sizeof(sql), sql,
			"SELECT COUNT(*) FROM dbf_%s;",
			table);
	rv = sqlite3_prepare(db, sql, -1, &dbs, NULL);
	if (rv != SQLITE_OK) {
		debug_printf(DBGLOG_ERROR,"Failed to read row count: %s\n",
				sqlite3_errmsg(db));
		free(dbf_name);
		free(dbt_name);
		fclose(dbf);
		if (header_res2)
			free(header_res2);
		return -1;
	}
	sqlite3_reset(dbs);
	rv = sqlite3_step(dbs);
	if (rv != SQLITE_ROW) {
		if (rv == SQLITE_DONE) {
			debug_printf(DBGLOG_ERROR,"There is no table %s.\n",
					table);
		} else {
			debug_printf(DBGLOG_ERROR,"SQL Query\n\t%s\n\t failed "
					"with error %s.\n",
					sql, sqlite3_errmsg(db));
		}
		free(dbf_name);
		free(dbt_name);
		fclose(dbf);
		if (header_res2)
			free(header_res2);
		return -1;
	}
	header.records = sqlite3_column_int(dbs, 0);

	/* Calculate field count */
	sqlite3_snprintf(sizeof(sql), sql,
			"SELECT COUNT(*) FROM fieldspec WHERE tablename=%Q;",
			table);
	rv = sqlite3_prepare(db, sql, -1, &dbs, NULL);
	if (rv != SQLITE_OK) {
		debug_printf(DBGLOG_ERROR,"Failed to read column count: %s\n",
				sqlite3_errmsg(db));
		free(dbf_name);
		free(dbt_name);
		fclose(dbf);
		if (header_res2)
			free(header_res2);
		return -1;
	}
	sqlite3_reset(dbs);
	rv = sqlite3_step(dbs);
	if (rv != SQLITE_ROW) {
		debug_printf(DBGLOG_ERROR,"%s returned unexpected result %d\n",
			sql, rv);
		free(dbf_name);
		free(dbt_name);
		fclose(dbf);
		if (header_res2)
			free(header_res2);
		return -1;
	}
	field_count = sqlite3_column_int(dbs, 0);
	debug_printf(DBGLOG_DETAIL,"There are %lu columns to export\n", (unsigned long)field_count);
	field = calloc(field_count, sizeof(struct dbf_field_desc));
	if (!field) {
		debug_printf(DBGLOG_ERROR,"Failed to allocate room for field metadata.\n");
		free(dbf_name);
		free(dbt_name);
		fclose(dbf);
		if (header_res2)
			free(header_res2);
		return -1;
	}

	/* Check for the existence of row ordering information */
	sqlite3_snprintf(sizeof(sql), sql,
			"PRAGMA table_info(dbf_%s);",
			table);
	rv = sqlite3_prepare(db, sql, -1, &dbs, NULL);
	if (rv != SQLITE_OK) {
		debug_printf(DBGLOG_ERROR,"Failed to check existance of table: %s\n",
				sqlite3_errmsg(db));
		free(dbf_name);
		free(dbt_name);
		fclose(dbf);
		if (header_res2)
			free(header_res2);
		return -1;
	}
	sqlite3_reset(dbs);
	rv = sqlite3_step(dbs);
	while (rv == SQLITE_ROW) {
		const char* colname = (const char*)sqlite3_column_text(dbs, 1);
		if (colname[0] != '_')
			break;

		if (strcmp(colname, "_rownum_") == 0) {
			ordered_table = 1;
			break;
		}
		rv = sqlite3_step(dbs);
	}

	header.record_size = 1;	// Count deletion flag
	sqlite3_snprintf(sizeof(sql), sql,
			"SELECT "
				"fieldname, "
				"fieldtype, "
				"fieldaddr, "
				"fieldlen, "
				"fieldcount, "
				"fieldmultiuser1, "
				"fieldworkarea, "
				"fieldmultiuser2, "
				"fieldsetflag, "
				"fieldreserved, "
				"fieldindex"
			" FROM fieldspec WHERE tablename=%Q"
			" ORDER BY tablecol ASC;",
			table);
	rv = sqlite3_prepare(db, sql, -1, &dbs, NULL);
	if (rv != SQLITE_OK) {
		debug_printf(DBGLOG_ERROR,"Failed to read field specification for table: %s\n",
				sqlite3_errmsg(db));
		free(dbf_name);
		free(dbt_name);
		fclose(dbf);
		if (header_res2)
			free(header_res2);
		return -1;
	}
	sqlite3_reset(dbs);
	rv = sqlite3_step(dbs);
	{
		struct dbf_field_desc*	fp = field;
		while (rv == SQLITE_ROW) {
			/* Field name */
			const char* name = (const char*)
				sqlite3_column_text(dbs, 0);
			strncpy((char*)fp->name, name,
					sizeof(fp->name));
			fp->name[10] = 0;
			debug_printf(DBGLOG_DETAIL,"Creating field %s\n", name);

			/* Field type */
			const char* type =  (const char*)
				sqlite3_column_text(dbs, 1);
			fp->type = type[0];

			if (fp->type == DBF_FIELD_TYPE_MEMO)
				enable_memo = 1;

			/* Address */
			fp->field_addr	= sqlite3_column_int(dbs, 2);

			/* Length */
			fp->length	= sqlite3_column_int(dbs, 3);
			header.record_size += fp->length;

			/* Decimal count */
			fp->count	= sqlite3_column_int(dbs, 4);

			/* Multi user field 1 */
			size_t ssize = sqlite3_column_bytes(dbs, 5);
			size_t dsize = sizeof(fp->multiuser1);
			memcpy(	fp->multiuser1,
				sqlite3_column_blob(dbs, 5),
				ssize < dsize ? ssize : dsize);

			/* Work area */
			fp->work_area = sqlite3_column_int(dbs, 6);

			/* Multi user field 2 */
			ssize = sqlite3_column_bytes(dbs, 7);
			dsize = sizeof(fp->multiuser2);
			memcpy(	fp->multiuser2,
				sqlite3_column_blob(dbs, 7),
				ssize < dsize ? ssize : dsize);
			
			/* Set flag */
			fp->set_flag = sqlite3_column_int(dbs, 8);

			/* Reserved bytes */
			ssize = sqlite3_column_bytes(dbs, 9);
			dsize = sizeof(fp->reserved);
			memcpy(	fp->reserved,
				sqlite3_column_blob(dbs, 9),
				ssize < dsize ? ssize : dsize);
			
			/* Index flag */
			fp->index = sqlite3_column_int(dbs, 10);

			/* Convert to disk representation */
			dbf_field_desc_to_disk(fp);
			fp++;
			rv = sqlite3_step(dbs);
		}
	}
	if ((rv != SQLITE_ROW) && (rv != SQLITE_DONE)) {
		debug_printf(DBGLOG_ERROR,"SQL Query\n\t%s\n\t failed "
				"with error %s.\n",
				sql, sqlite3_errmsg(db));
		free(dbf_name);
		free(dbt_name);
		fclose(dbf);
		if (header_res2)
			free(header_res2);
		return -1;
	}

	if (!got_tablespec) {
		/* Determine version field */
		header.version		= enable_memo
			? DBF_VER_V4DBT : DBF_VER_V4;
		/* Set other defaults */
		header.language		= 0x1a;
	}

	/* Calculate header size */
	header.header_size	= sizeof(header)
				+ (field_count * sizeof(struct dbf_field_desc))
				/* Terminator */
				+ 1
				/* Database Container */
				+ (enable_dbc ? DBF_DBC_LEN : 0)
				/* Any reserved data after that */
				+ header_res2_sz;

	/* We can now write the full header */
	dbf_table_header_to_disk(&header);
	fwrite(&header, sizeof(header), 1, dbf);
	dbf_table_header_to_host(&header);
	/* and field specification blocks */
	fwrite(field, sizeof(struct dbf_field_desc), field_count, dbf);
	/* Write header terminator */
	fputc(DBF_HEAD_TERM, dbf);
	/* Write the DBC if enabled */
	if (enable_dbc)
		fwrite(dbc, sizeof(dbc), 1, dbf);
	/* Write any reserved data after this */
	if (header_res2_sz && header_res2)
		fwrite(header_res2, header_res2_sz, 1, dbf);

	/* Convert fields back to host representation */
	{
		int fc = field_count;
		struct dbf_field_desc*	fp = field;
		while(fc) {
			dbf_field_desc_to_host(fp);
			fp++;
			fc--;
		}
	}

	if (enable_memo) {
		dbt = fopen(dbt_name, "wb+");
		if (!dbt) {
			debug_printf(DBGLOG_ERROR, "Could not open DBT file %s: %s",
					dbt_name, strerror(errno));
			free(dbf_name);
			free(dbt_name);
			fclose(dbf);
			if (header_res2)
				free(header_res2);
			return errno;
		}
		/* Generate a dummy DBT header for now */
		memset(buffer, 0, sizeof(buffer));
		fwrite(buffer, 512, 1, dbt);
	}

	int i = 0, records = 0;

	/* Query the table for records */
	if (ordered_table)
		sqlite3_snprintf(sizeof(sql), sql,
			"SELECT * FROM dbf_%s ORDER BY _rownum_ ASC;", table);
	else
		sqlite3_snprintf(sizeof(sql), sql,
			"SELECT * FROM dbf_%s;", table);

	rv = sqlite3_prepare(db, sql, -1, &dbs, NULL);
	if (rv != SQLITE_OK) {
		debug_printf(DBGLOG_ERROR, "Failed to prepare read from %s: %s\n",
				table, sqlite3_errmsg(db));
		free(dbf_name);
		free(dbt_name);
		fclose(dbf);
		if (dbt)
			fclose(dbt);
		if (header_res2)
			free(header_res2);
		return -1;
	}
	sqlite3_reset(dbs);
	rv = sqlite3_step(dbs);
	while(rv == SQLITE_ROW) {
		/* Read in the deleted flag and write to DBF */
		size_t field_offset = ordered_table ? 2 : 1;
		const unsigned char* f = sqlite3_column_text(dbs, 0);
		if (f && f[0] && (f[0] != ' '))
			fputc('*', dbf);
		else
			fputc(' ', dbf);

		/* We ignore _rownum_: it is there to sort the rows */

		/* 
		 * Now read in each column and write out to DBF in order,
		 * truncating to the field width as needed.
		 */
		for (i = 0; i < field_count; i++) {
			int field_id = i + field_offset;
			memset(buffer, ' ', sizeof(buffer));
			switch (field[i].type) {
				case DBF_FIELD_TYPE_CHAR:
				case DBF_FIELD_TYPE_LOGICAL:
					writeText(dbs, field_id,
							&field[i], dbf);
					break;
				case DBF_FIELD_TYPE_NUMBER:
					writeNumber(dbs, field_id,
							&field[i], dbf);
					break;
				case DBF_FIELD_TYPE_DATE:
					writeDate(dbs, field_id,
							&field[i], dbf);
					break;
				case DBF_FIELD_TYPE_MEMO:
					writeMemo(dbs, field_id,
							&field[i], dbf, dbt,
							dbth.version,
							&dbt_usage);
					break;
			}
		}
		rv = sqlite3_step(dbs);
		records++;
	}

	/* Write the end-of-file marker */
	fputc(DBF_EOF, dbf);

	/* File length */
	long length = ftell(dbf);
	fclose(dbf);

	/* Truncate to size */
	if (truncate(dbf_name, length)) {
		debug_printf(DBGLOG_WARNING,"Truncate of DBF file failed.  "
				"Garbage follows the data.: %s", strerror(errno));
	}
	debug_printf(DBGLOG_INFO,"%s: %d records from database written.  Table size %ld bytes.\n",
			dbf_name, records, length);
	
	/* Fix up header block in DBT */
	if (dbt) {
		dbth.next		= dbt_usage;
		dbth.version		= 0x0;
		dbth.block_size		= 0;	/* Why? */
		dbth.block_length	= 512;
		strncpy(dbth.dbf_name, table, sizeof(dbth.dbf_name));

		fseek(dbt, 0, SEEK_SET);
		dbt_header_to_disk(&dbth);
		fwrite(&dbth, sizeof(dbth), 1, dbt);
		fclose(dbt);
		debug_printf(DBGLOG_INFO,"%s: %lu blocks from database written.\n",
			dbt_name, (unsigned long)dbt_usage);
	}

	free(dbf_name);
	free(dbt_name);
	if (header_res2)
		free(header_res2);
	return 0;
}

void writeAllTables(sqlite3* db, const char* dir) {
	sqlite3_stmt* dbs;
	int rv = sqlite3_prepare(db,
			"SELECT DISTINCT tablename FROM fieldspec",
			-1, &dbs, NULL);
	if (rv != SQLITE_OK) {
		debug_printf(DBGLOG_CRITICAL,
				"Failed to retrieve table list: %s\n",
				sqlite3_errmsg(db));
		return;
	}
	sqlite3_reset(dbs);
	rv = sqlite3_step(dbs);
	while(rv == SQLITE_ROW) {
		/* Read in the table name */
		const char* table = (const char*)sqlite3_column_text(dbs, 0);
		if (writeTable(db, dir, table)) {
			sqlite3_close(db);
			return;
		}
		rv = sqlite3_step(dbs);
	}
}

int main( int argc, char** argv ) {
	const char* prog_name = argv[0];
	argv++; argc--;

	debug_log = stdout;
	while(argc && argv[0][0] == '-') {
		if ((argv[0][0] == '-') &&
				((argv[0][1] == 'q') || (argv[0][1] == 'v'))) {
			const char* a = &argv[0][1];
			while (*a) {
				if (*a == 'q')
					debug_level--;
				else if (*a == 'v')
					debug_level++;
				a++;
			}
		} else if (!strcmp(argv[0], "-n")) {
			debug_level = atoi(argv[1]);
			argc--; argv++;
		} else if (!strcmp(argv[0], "-l")) {
			FILE* old_log = debug_log;
			debug_log = fopen(argv[1],"a");
			if (!debug_log) {
				if (!old_log)
					debug_log = stderr;
				debug_printf(DBGLOG_CRITICAL,
						"Unable to open log %s: %s\n",
						argv[1], strerror(errno));
				debug_log = old_log;
			} else if (old_log) {
				fclose(old_log);
			}
			argc--; argv++;
		}
		argc--; argv++;
	}

	if (argc < 3) {
		debug_printf(DBGLOG_INFO,
				"Usage: %s [-v] [-q] [-l file.log]"
				" [-n log_level] sqlite.db dir "
				"{table [... table] | --all}\n",
				prog_name);
		return -1;
	}

	const char* db_name = argv[0];
	argc--; argv++;

	const char* dir = argv[0];
	argc--; argv++;

	sqlite3* db;
	int rv = sqlite3_open(db_name, &db);
	if (rv != SQLITE_OK) {
		if (db) {
			debug_printf(DBGLOG_CRITICAL,
					"Opening %s failed: %s\n",
					db_name, sqlite3_errmsg(db));
			sqlite3_close(db);
		} else {
			debug_printf(DBGLOG_CRITICAL,
					"Failed to open SQLite database: %s\n",
					db_name);
		}
		return -1;
	}
	while(argc) {
		if (!strcmp(argv[0], "--all")) {
			writeAllTables(db, dir);
			break;
		}

		char* table = strdup(argv[0]);
		if (!table) {
			debug_printf(DBGLOG_CRITICAL,
					"Failed to allocate memory.\n");
			return -1;
		} else {
			/* Lop off any extensions, and ignore non-DBF files */
			char* ext = rindex(table, '.');
			if (ext) {
				if (!strcasecmp(ext, ".dbf")) {
					*ext = 0;
				} else {
					debug_printf(DBGLOG_WARNING,
							"Ignoring non-table argument %s\n", table);
					argv++;
					argc--;
					continue;
				}
			}
		}

		if (writeTable(db, dir, table)) {
			debug_printf(DBGLOG_CRITICAL,
					"Failed at table %s\n", argv[0]);
			sqlite3_close(db);
			return -1;
		}
		free(table);
		argv++;
		argc--;
	}
	sqlite3_close(db);

	return 0;
}
