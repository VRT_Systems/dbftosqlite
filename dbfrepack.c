/*
 *  DBF to SQLite conversion tool
 *  Copyright (C) 2011  Stuart Longland <me@vk4msl.yi.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>

#include "dbf.h"

int main( int argc, char** argv ) {
	if (argc < 2) {
		fprintf(stderr,"Usage: %s file.dbf\n",argv[0]);
		return -1;
	}
	FILE* dbf = fopen(argv[1], "rb+");
	if (!dbf) {
		perror("Could not open dbf file");
		return errno;
	}

	struct dbf_table_header header;

	size_t read = fread(&header, sizeof(header), 1, dbf);
	dbf_table_header_to_host(&header);
	int records = 0;
	int skipped = 0;
	if (!read) {
		fprintf(stderr,"Short read of header\n");
	}
	
	/* 
	 * Next are the fields, we'll skip these and go straight to the
	 * records for repacking.
	 */
	fseek(dbf, header.header_size, SEEK_SET);

	/* 
	 * Note the read and write positions.  They are the same for now,
	 * but the read will advance ahead of the write position when deleted
	 * records are encountered.
	 */
	long read_pos = ftell(dbf);
	long write_pos = read_pos;

	/* Finally, there are the records */
	int rec = 0;
	while(!feof(dbf)) {
		int del = fgetc(dbf);
		if (del == EOF)
			break;
		if (del == DBF_EOF)
			break;
		if (del == DBF_RECORD_DELETED) {
			/* Skip this record and move on */
			read_pos += header.record_size;
			fseek(dbf, read_pos, SEEK_SET);
			skipped++;
			printf("Record %d skipped\n", rec);
		} else {
			records++;
			/* If read and write pointers match, advance both */
			if (read_pos == write_pos) {
				read_pos += header.record_size;
				write_pos = read_pos;
				fseek(dbf, read_pos, SEEK_SET);
				printf("Record %d left in-place\n", rec);
			} else {
				/* Read the record in, then write it here */
				int length = header.record_size;
				printf("Record %d to be relocated\n", rec);
				while(length && !feof(dbf)) {
					uint8_t buffer[1024];
					
					fseek(dbf, read_pos, SEEK_SET);
					size_t read = fread(buffer, 1,
						length > sizeof(buffer)
							? sizeof(buffer) 
							: length,
						dbf);
					if (!read)
						break;

					printf("\tread %ld from 0x%08lx\n",
						(unsigned long)read, read_pos);
					read_pos = ftell(dbf);

					fseek(dbf, write_pos, SEEK_SET);
					size_t written = fwrite(buffer,
						1, read, dbf);
					printf("\twrote %ld to 0x%08lx\n",
						(unsigned long)written,
						write_pos);
					write_pos = ftell(dbf);
					
					if (read != written)
						fprintf(stderr, "WARNING! "
							"Read %ld bytes, "
							"wrote %ld bytes.\n",
							(unsigned long)read,
							(unsigned long)written);
					length -= read;
				}
			}
		}
		printf("Read: 0x%08lx / Write 0x%08lx\n",
				(unsigned long)read_pos,
				(unsigned long)write_pos);
		rec++;
	}

	/* Write the end-of-file marker */
	fseek(dbf, write_pos, SEEK_SET);
	fputc(DBF_EOF, dbf);

	/* File length */
	long length = ftell(dbf);

	/* Update number of records */
	fseek(dbf, 0, SEEK_SET);
	header.records = records;
	fwrite(&header, sizeof(header), 1, dbf);

	fclose(dbf);

	/* Truncate to size */
	if (truncate(argv[1], length)) {
		perror("Truncate failed:  Garbage follows the data.");
	}

	printf("%s: %d records skipped, %d records written.  "
		"New size %ld bytes.\n", argv[1], skipped, records, length);
	return 0;
}
