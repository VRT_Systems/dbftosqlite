#    DBF to SQLite conversion tool
#    Copyright (C) 2011  Stuart Longland <me@vk4msl.yi.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

SQLITE_CPPFLAGS=$(shell pkg-config --cflags sqlite3)
SQLITE_LIBS=$(shell pkg-config --libs sqlite3)

CPPFLAGS := ${CPPFLAGS} -Wall -Werror
LDFLAGS := ${LDFLAGS}

.PHONY: all clean deb
all:	dbftosqlite sqlitetodbf dbfrepack
clean:
	-rm -f dbftosqlite sqlitetodbf dbfrepack

dbftosqlite sqlitetodbf : CPPFLAGS+=$(SQLITE_CPPFLAGS)
dbftosqlite sqlitetodbf : LDFLAGS+=$(SQLITE_LIBS)

%: %.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ $< $(LDFLAGS)

%.o: %.c dbf.h

DEB_VERSION=$(shell dpkg-parsechangelog | grep ^Version: \
		| cut -f 2 -d: | sed -e 's/ //g')
DEB_SRC=dbftosqlite-$(DEB_VERSION)

deb:
	mkdir $(DEB_SRC)
	cp -a $(filter-out $(DEB_SRC),$(wildcard *)) $(DEB_SRC)
	cd $(DEB_SRC); dpkg-buildpackage $(DPKG_BUILDPKG_ARGS)
	rm -fr $(DEB_SRC)
