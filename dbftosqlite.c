/*
 *  DBF to SQLite conversion tool
 *  Copyright (C) 2011  Stuart Longland <me@vk4msl.yi.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <sqlite3.h>

#include "dbf.h"
#include "debug.h"

void dumpChar( const struct dbf_field_desc* field, char* buffer,
		char* sql_part, size_t sql_part_sz ) {
	/* Strip trailing spaces */
	int p;
	for (p = field->length - 1; p >= 0; p--)
		if (buffer[p] == ' ')
			buffer[p] = 0;
		else
			break;

	debug_printf(DBGLOG_DEBUG_L, "Char Field    %s = \"%s\".\n",
			field->name, buffer);
	sqlite3_snprintf(sql_part_sz-1,
			sql_part,
			", %Q",
			buffer);
}

void dumpNumber( const struct dbf_field_desc* field, const char* buffer,
		char* sql_part, size_t sql_part_sz ) {
	const char* p = buffer;
	const char* q;
	uint8_t valid = 0;
	while(*p == ' ')
		p++;
	/* Check for at least one digit, to handle the "    .  " case */
	q = p;
	while(*q) {
		/* Is this a digit? */
		if ((*q >= '0') && (*q <= '9')) {
			/* Yes */
			valid = 1;
			break;
		}
		q++;
	}


	debug_printf(DBGLOG_DEBUG_L, "Numeric Field %s = \"%s\" valid=%d.\n",
			field->name, p, valid);
	if (valid && *p)
		sqlite3_snprintf(sql_part_sz-1,
				sql_part,
				", %s",
				p);
	else
		sqlite3_snprintf(sql_part_sz-1,
				sql_part,
				", NULL");
}

void dumpLogical( const struct dbf_field_desc* field, const char* buffer,
		char* sql_part, size_t sql_part_sz ) {
	debug_printf(DBGLOG_DEBUG_L, "Logical Field %s = %c.\n",
			field->name, buffer[0]);
	sqlite3_snprintf(sql_part_sz,
			sql_part,
			", '%c'",
			buffer[0]);
}

void dumpDate( const struct dbf_field_desc* field, const char* buffer,
		char* sql_part, size_t sql_part_sz ) {
	/* We don't bother enforcing length here */
	sql_part[ 0] = ',';
	sql_part[ 1] = ' ';
	sql_part[ 2] = '\'';
	sql_part[ 3] = buffer[0];
	sql_part[ 4] = buffer[1];
	sql_part[ 5] = buffer[2];
	sql_part[ 6] = buffer[3];
	sql_part[ 7] = '-';
	sql_part[ 8] = buffer[4];
	sql_part[ 9] = buffer[5];
	sql_part[10] = '-';
	sql_part[11] = buffer[6];
	sql_part[12] = buffer[7];
	sql_part[13] = '\'';
	sql_part[14] = 0;

	debug_printf(DBGLOG_DEBUG_L, "Date Field %s = \"%s\".\n",
			field->name, &sql_part[2]);
}

void dumpMemo( const struct dbf_field_desc* field, char* buffer,
		size_t buffer_sz, char* sql_part, size_t sql_part_sz,
		FILE* dbt, const struct dbt_header* dbth ) {
	if (dbt) {
		debug_printf(DBGLOG_DEBUG_L, "Memo Field    %s = \"%s\".\n",
				field->name, buffer);
		/* Read from n-th block */
		int block_id = atoi(buffer);
		if (block_id) {
			debug_printf(DBGLOG_DEBUG_M, "Looking up block %d in DBT\n", block_id);
			char* dp = buffer;
			size_t dr = buffer_sz;
			memset(buffer, 0, dr);

			if (!fseek(dbt, block_id*dbth->block_length,
						SEEK_SET)) {
				debug_printf(DBGLOG_DEBUG_M,
						"Seeked to block %d in DBT\n", block_id);
				if (dbth->version == DBT_VER3) {
					uint8_t block[512];
					size_t sr = sizeof(block);
					while (fread(block, sr, 1, dbt)) {
						const uint8_t* sp = block;
						while(dr && sr && (*sp != DBT_EOB)) {
							*dp = *sp;
							dp++;
							sp++;
							dr--;
							sr--;
						}
						if (*sp == DBT_EOB)
							break;
						sr = sizeof(block);
					}
				} else {
					union dbt_block_header blockh;
					if(fread(&blockh, sizeof(blockh), 1, dbt)) {
						dbt_block_header_to_host(&blockh);
						debug_printf(DBGLOG_DEBUG_L,
								"Block magic: 0x%08x\n",
								blockh.v4used.magic);
						if (blockh.v4used.magic == DBT_USED_MAGIC) {
							size_t fr = blockh.v4used.length;
							debug_printf(DBGLOG_DEBUG_L,
									"Block contains %lu bytes of data\n",
									(unsigned long)(fr));

							/* The absolute maximum we can read in is buffer_sz */
							if (fr > buffer_sz)
								fr = buffer_sz;

							if (!fread(dp, 1, fr, dbt))
								perror("Failed to read block data");
							/* Check for a broken double-header */
							memcpy(&blockh, dp, sizeof(blockh));
							dbt_block_header_to_host(&blockh);
							if (blockh.v4used.magic == DBT_USED_MAGIC) {
								debug_printf(DBGLOG_DEBUG_H,
										"DOUBLE HEADER DETECTED!!  "
										"Stripped bogus header.\n");
								memmove(dp, dp + sizeof(blockh), fr-sizeof(blockh));
							}
						} else {
							debug_printf(DBGLOG_DEBUG_L, "Block is unused.  No data.\n");
						}
					} else {
						debug_printf(DBGLOG_ERROR, "Failed to read block header: %s",
								strerror(errno));
					}
				}
				/* Strip off trailing 0x0d 0x0a */
				{
					int end = strlen(buffer) - 1;
					while((end >= 0) && (buffer[end] == 0))
						end--;

					if ((buffer[end-1] == DBT_BE0) && (buffer[end] == DBT_BE1))
						buffer[end-1] = 0;
				}
				/* Translate 0x8d 0x0a to newline */
				{
					const char* in = buffer;
					char* out = buffer;
					while (*in) {
						if ((in[0] == DBT_LE0) && (in[1] == DBT_LE1)) {
							*out	= '\n';
							in	+= 2;
							out++;
						} else {
							if (out != in)
								*out	= *in;
							in++;
							out++;
						}
					}
				}

				debug_printf(DBGLOG_DEBUG_L, "MEMO Content:\n%s\nEnd of MEMO\n", buffer);
				sqlite3_snprintf(
						sql_part_sz,
						sql_part,
						", %Q",
						buffer);
			} else {
				sqlite3_snprintf(
						sql_part_sz,
						sql_part,
						", 'DBT SEEK FAILED for block %d'",
						block_id);
			}
		} else {
			strcpy(sql_part, ", ''");
		}
	} else {
		sqlite3_snprintf(
				sql_part_sz,
				sql_part,
				", 'NO MEMO (id=%q)'",
				buffer);
	}
}
void dumpBlob( const struct dbf_field_desc* field, char* buffer,
		char* sql_part, size_t sql_part_sz ) {
	char* sp = buffer;
	char* dp = &sql_part[4];
	size_t l = field->length;
	sql_part[0] = ',';
	sql_part[1] = ' ';
	sql_part[2] = 'x';
	sql_part[3] = '\'';
	while(l) {
		uint8_t high = *sp >> 4;
		uint8_t low = *sp & 0x0f;
		*dp = (high > 9)
			? ('a' + (high - 10))
			: ('0' + high);
		dp++;
		*dp = (low > 9)
			? ('a' + (low - 10))
			: ('0' + low);
		dp++;
		sp++;
		l--;
	}
	*dp = '\'';
}

int dumpTable( sqlite3* db, const char* dbf_name ) {
	FILE* dbf;
	FILE* dbt;
	sqlite3_stmt* dbs;
	char sql[16384];
	char table[32];
	char* dbt_name = strdup(dbf_name);

	struct dbf_table_header header;
	struct dbf_field_desc*	field	= NULL;
	uint8_t fields 			= 0;
	int rv;

	struct dbt_header	dbth;

	/* Database Container for Visual FoxPro */
	uint8_t dbc[DBF_DBC_LEN];
	uint8_t has_dbc = 0;

	/* Remaining blob data */
	uint8_t* remainder = NULL;
	size_t remainder_sz = 0;

	memset(sql, 0, sizeof(sql));
	memset(&dbth, 0, sizeof(dbth));

	{
		char* dot = rindex(dbt_name, '.');
		/* Assume .dbf follows; replace f with t */
		if (dot)
			dot[3] = 't';
		else {
			free(dbt_name);
			dbt_name = NULL;
		}
	}

	dbf = fopen(dbf_name, "rb");
	if (!dbf) {
		perror("Could not open dbf file");
		free(dbt_name);
		return errno;
	}

	size_t read = fread(&header, sizeof(header), 1, dbf);
	if (!read) {
		debug_printf(DBGLOG_ERROR, "Short read of header: %s.\n",
				errno ? strerror(errno) : "no error given");
	}
	dbf_table_header_to_host(&header);
	debug_printf(DBGLOG_DETAIL,
			"dBase file %s -- HEADER\n"
			"Version signature:\t\t0x%02x\n"
			"Date of last modification:\t%04d/%02d/%02d\n"
			"Number of records:\t\t%d\n"
			"Header length:\t\t\t%d\n"
			"Record length:\t\t\t%d\n"
			"Reserved bytes:\t\t\t0x%04x\n"
			"Incomplete transaction:\t\t%s (0x%02x)\n"
			"Encrypted:\t\t\t%s (0x%02x)\n"
			"Free record thread:\t\t0x%08x\n"
			"Multi-user reserved:\t\t{0x%02x,0x%02x,0x%02x,0x%02x,\n"
			"\t\t\t\t 0x%02x,0x%02x,0x%02x,0x%02x}\n"
			"MDX Flag:\t\t\t0x%02x\n"
			"Language driver:\t\t0x%02x\n"
			"Reserved bytes:\t\t\t0x%04x\n",
			dbf_name,
			header.version,
			header.last_update.year + 1900,
			header.last_update.month,
			header.last_update.day,
			header.records,
			header.header_size,
			header.record_size,
			header.reserved1,
			(header.incomplete ? "Yes" : "No"),
			header.incomplete,
			(header.encryption ? "Yes" : "No"),
			header.encryption,
			header.free_record,
			header.multiuser[0], header.multiuser[1],
			header.multiuser[2], header.multiuser[3],
			header.multiuser[4], header.multiuser[5],
			header.multiuser[6], header.multiuser[7],
			header.mdx,
			header.language,
			header.reserved2);

	/* Try opening a DBT of the same name */
	if (dbt_name) {
		dbt = fopen(dbt_name, "rb");
		if (!dbt) {
			perror("Could not open dbt file.  Memo fields disabled.");
			free(dbt_name);
			dbt_name = NULL;
		} else {
			read = fread(&dbth, sizeof(dbth), 1, dbt);
			dbt_header_to_host(&dbth);
			if (dbth.version == DBT_VER3)
			{
				debug_printf(DBGLOG_DETAIL, 
					"DBase III Memo file detected\n"
					"\tNext Free Block:\t%d\n", dbth.next);
				dbth.block_length = 512;
			}
			else if (dbth.version == DBT_VER4)
			{
				debug_printf(DBGLOG_DETAIL, 
					"DBase IV Memo file detected\n"
					"\tNext Free Block:\t%d\n"
					"\tBlock Size:\t%d bytes\n"
					"\tDBF Name:\t%s.dbf\n"
					"\tBlock Length:\t%d bytes\n",
					dbth.next,
					dbth.block_size,
					dbth.dbf_name,
					dbth.block_length);
			}
			else
			{
				debug_printf(DBGLOG_ERROR, 
					"UNKNOWN DBase MEMO FILE!  Assuming IV.\n");
			}
		}
	} else {
		debug_printf(DBGLOG_ERROR, "No memo field file available.");
		dbt = NULL;
	}

	rv = sqlite3_prepare(db,
			"BEGIN;", -1,
			&dbs, NULL);
	if (rv == SQLITE_OK)
		sqlite3_step(dbs);

	/*
	 * We create a SQL table, which is the base name of the file minus
	 * anything after the first dot.
	 */
	{
		char* p = rindex(dbf_name, '/');
		if (!p)
			strncpy(table, dbf_name, sizeof(table)-1);
		else
			strncpy(table, &p[1], sizeof(table)-1);
		p = index(table, '.');
		if (p)
			*p = 0;
	}
	snprintf(sql, sizeof(sql)-1,
		"DROP TABLE dbf_%s;", table);
	rv = sqlite3_prepare(db,
			sql, -1,
			&dbs, NULL);
	debug_printf(DBGLOG_DETAIL, "Dropping table dbf_%s\n", table);
	if (rv == SQLITE_OK)
		sqlite3_step(dbs);

	sqlite3_snprintf(sizeof(sql)-1, sql,
		"DELETE FROM fieldspec WHERE tablename=%Q;", table);
	rv = sqlite3_prepare(db,
			sql, -1,
			&dbs, NULL);
	debug_printf(DBGLOG_DETAIL, "Clearing metadata for table %s\n", table);
	if (rv == SQLITE_OK)
		sqlite3_step(dbs);

	snprintf(sql, sizeof(sql)-1,
		"CREATE TABLE dbf_%s ("
		"_delete_ TEXT,"
		"_rownum_ INTEGER", table);

	/* 
	 * Following this should be the fields.  If we hit a field with a
	 * name starting with 0x0d, stop there, that's the end of the header.
	 */
	while(!feof(dbf)) {
		struct dbf_field_desc f;
		struct dbf_field_desc* fptr;
		char sql_part[1024];
		memset(sql_part, 0, sizeof(sql_part));

		read = fread(&f, sizeof(f), 1, dbf);
		dbf_field_desc_to_host(&f);
		if (!read) {
			debug_printf(DBGLOG_ERROR, "Short read of field\n");
		}
		if (f.name[0] == DBF_HEAD_TERM) {
			debug_printf(DBGLOG_DETAIL, "End of fields reached\n");
			/* Seek back to just past the terminator */
			fseek(dbf, 1-sizeof(f), SEEK_CUR);
			if ((header.version == DBF_VER_VFP) ||
					(header.version == DBF_VER_VFPAI)) {
				/* This is Visual FoxPro */
				debug_printf(DBGLOG_DETAIL,
						"Visual FoxPro detected, "
						"preserving database "
						"container.\n");
				if(fread(dbc, sizeof(dbc), 1, dbf))
					has_dbc = 1;
			}

			remainder_sz = ftell(dbf);
			if (remainder_sz != header.header_size) {
				
				/* There's remaining data */
				remainder_sz = header.header_size - remainder_sz;
				remainder = malloc(remainder_sz);
				if (remainder) {
					if(!fread(remainder, remainder_sz, 1,
								dbf)) {
						debug_printf(DBGLOG_ERROR, 
								"Unable to read %lu "
								"bytes for reserved "
								"data.\n",
							(unsigned long)remainder_sz);
					}
				} else {
					debug_printf(DBGLOG_ERROR,
							"Unable to allocate "
							"%lu bytes for "
							"reserved data.  "
							"Ignoring!\n",
							(unsigned long)remainder_sz);
					remainder_sz = 0;
					fseek(dbf, header.header_size, SEEK_SET);
				}
			}
			break;
		}

		/* Allocate memory for new field */
		fields++;
		if (field) {
			fptr = realloc(field, sizeof(f)*fields);
		} else {
			fptr = malloc(sizeof(f));
		}

		if (fptr) {
			field = fptr;
			memcpy(&field[fields-1], &f, sizeof(f));
		} else {
			debug_printf(DBGLOG_ERROR,
					"Cannot allocate memory for fields: %s",
					strerror(errno));
			if (remainder)
				free(remainder);
			if (dbt_name)
				free(dbt_name);
			if (dbt)
				fclose(dbt);
			fclose(dbf);
			return -1;
		}

		snprintf(sql_part, sizeof(sql_part)-1,
			"INSERT OR REPLACE INTO fieldspec ("
				"tablename, "
				"tablecol, "
				"fieldname, "
				"fieldtype, "
				"fieldaddr, "
				"fieldlen, "
				"fieldcount, "
				"fieldmultiuser1, "
				"fieldworkarea, "
				"fieldmultiuser2, "
				"fieldsetflag, "
				"fieldreserved, "
				"fieldindex"
			") VALUES ("
				"'%s', "	/* Table name */
				"%d, "		/* Index */
				"'%s', "	/* Field name */
				"'%c', "	/* Field type */
				"%lu, "		/* Address */
				"%d, "		/* Length */
				"%d, "		/* Count */
				"x'%02x%02x',"	/* Multi user */
				"x'%02x',"	/* Work area */
				"x'%02x%02x',"	/* Multi user */
				"x'%02x',"	/* Set flag */
				"x'%02x%02x%02x%02x%02x%02x%02x',"
						/* Reserved */
				"%d"		/* Index */
			");",
			table,
			fields-1,
			f.name,
			f.type,
			(long unsigned int)(f.field_addr),
			f.length,
			f.count,
			f.multiuser1[0], f.multiuser1[1],
			f.work_area,
			f.multiuser2[0], f.multiuser2[1],
			f.set_flag,
			f.reserved[0], f.reserved[1],
			f.reserved[2], f.reserved[3],
			f.reserved[4], f.reserved[5],
			f.reserved[6],
			f.index);
		debug_printf(DBGLOG_DEBUG_L,  "SQL: %s\n", sql_part);
		rv = sqlite3_prepare(db,
			sql_part, -1,
			&dbs, NULL);
		debug_printf(DBGLOG_DETAIL, "Recording field %s\n", f.name);
		if (rv != SQLITE_OK) {
			debug_printf(DBGLOG_ERROR,
					"Failed to prepare to record field %s: %s\n",
					f.name, sqlite3_errmsg(db));
			if (remainder)
				free(remainder);
			if (dbt_name)
				free(dbt_name);
			if (dbt)
				fclose(dbt);
			fclose(dbf);
			return rv;
		}
		rv = sqlite3_step(dbs);
		if (rv != SQLITE_DONE) {
			fclose(dbf);
			if (remainder)
				free(remainder);
			if (dbt_name)
				free(dbt_name);
			if (dbt)
				fclose(dbt);
			debug_printf(DBGLOG_ERROR,
					"Failed to record field %s: %s\n",
					f.name, sqlite3_errmsg(db));
			return rv;
		}

		/* Generate next column statement */
		switch(f.type) {
			case DBF_FIELD_TYPE_CHAR:
				snprintf(sql_part, sizeof(sql_part)-1,
					", \"%s\" TEXT",
					f.name);
				break;
			case DBF_FIELD_TYPE_NUMBER:
				if (f.count)
					snprintf(sql_part, sizeof(sql_part)-1,
							", \"%s\" REAL",
							f.name);
				else
					snprintf(sql_part, sizeof(sql_part)-1,
							", \"%s\" INTEGER",
							f.name);
				break;
			case DBF_FIELD_TYPE_LOGICAL:
				snprintf(sql_part, sizeof(sql_part)-1,
					", \"%s\" TEXT",
					f.name);
				break;
			case DBF_FIELD_TYPE_DATE:
				snprintf(sql_part, sizeof(sql_part)-1,
					", \"%s\" TEXT",
					f.name);
				break;
			case DBF_FIELD_TYPE_MEMO:
				snprintf(sql_part, sizeof(sql_part)-1,
					", \"%s\" TEXT",
					f.name);
				break;
			default:
				snprintf(sql_part, sizeof(sql_part)-1,
					", \"%s\" BLOB",
					f.name);
		}
		strncat(sql, sql_part, sizeof(sql)-1);
	}
	strncat(sql, ");", sizeof(sql)-1);

	debug_printf(DBGLOG_DEBUG_L,  "SQL: %s\n", sql);
	rv = sqlite3_prepare(db,
			sql, -1,
			&dbs, NULL);
	debug_printf(DBGLOG_DETAIL, "Creating table dbf_%s\n", table);
	if (rv != SQLITE_OK) {
		fclose(dbf);
		if (remainder)
			free(remainder);
		if (dbt_name)
			free(dbt_name);
		if (dbt)
			fclose(dbt);
		debug_printf(DBGLOG_ERROR,
				"Failed to prepare creating table for %s: %s\n",
				table, sqlite3_errmsg(db));
		return rv;
	}
	rv = sqlite3_step(dbs);
	if (rv != SQLITE_DONE) {
		fclose(dbf);
		if (remainder)
			free(remainder);
		if (dbt_name)
			free(dbt_name);
		if (dbt)
			fclose(dbt);
		debug_printf(DBGLOG_ERROR,
				"Failed to create table for %s: %s\n",
				table, sqlite3_errmsg(db));
		return rv;
	}

	rv = sqlite3_prepare(db,
			"COMMIT;", -1,
			&dbs, NULL);
	if (rv == SQLITE_OK)
		sqlite3_step(dbs);

	rv = sqlite3_prepare(db,
			"BEGIN;", -1,
			&dbs, NULL);
	if (rv == SQLITE_OK)
		sqlite3_step(dbs);

	/* Finally, there are the records */
	size_t rownum = 0;
	while(!feof(dbf)) {
		int del = fgetc(dbf);
		char buffer[4096];
		char sql_part[8192];
		if (del == EOF)
			break;
		if (del == DBF_EOF)
			break;
		if (!del)
			del = (int)' ';
		int i;
		
		debug_printf(DBGLOG_DEBUG_H, "\tDelete flag: %d\n", del);
		snprintf(sql, sizeof(sql)-1,
			"INSERT INTO dbf_%s VALUES ('%c',%lu",
			table, (char)del, (unsigned long)rownum++);

		for(i = 0; i < fields; i++) {
			memset(sql_part, 0, sizeof(sql_part));
			memset(buffer, 0, sizeof(buffer));
			if (fread(buffer, field[i].length, 1, dbf) < 1) {
				perror("\t** WARNING **: Short read!\n");
			}

			debug_printf(DBGLOG_DEBUG_H, "\t%10s = \"%s\"\n", field[i].name, buffer);
			switch(field[i].type) {
				case DBF_FIELD_TYPE_CHAR:
					dumpChar(&field[i], buffer,
						sql_part, sizeof(sql_part));
					break;
				case DBF_FIELD_TYPE_NUMBER:
					dumpNumber(&field[i], buffer,
						sql_part, sizeof(sql_part));
					break;
				case DBF_FIELD_TYPE_LOGICAL:
					dumpLogical(&field[i], buffer,
						sql_part, sizeof(sql_part));
					break;
				case DBF_FIELD_TYPE_DATE:
					dumpDate(&field[i], buffer,
						sql_part, sizeof(sql_part));
					break;
				case DBF_FIELD_TYPE_MEMO:
					dumpMemo(&field[i], buffer,
						sizeof(buffer), sql_part,
						sizeof(sql_part),
						dbt, &dbth);
					break;
				default:
					dumpBlob(&field[i], buffer,
						sql_part, sizeof(sql_part));
			}
			strncat(sql, sql_part, sizeof(sql)-1);
		}
		strncat(sql, ");", sizeof(sql)-1);
		debug_printf(DBGLOG_DEBUG_L, "SQL: %s\n", sql);
		rv = sqlite3_prepare(db,
				sql, -1,
				&dbs, NULL);
		if (rv != SQLITE_OK) {
			fclose(dbf);
			if (remainder)
				free(remainder);
			if (dbt_name)
				free(dbt_name);
			if (dbt)
				fclose(dbt);
			debug_printf(DBGLOG_ERROR,
					"Failed to prepare to insert row: %s\n",
					sqlite3_errmsg(db));
			return rv;
		}
		debug_printf(DBGLOG_DEBUG_L, "Executing SQL\n");
		rv = sqlite3_step(dbs);
		if (rv != SQLITE_DONE) {
			fclose(dbf);
			if (remainder)
				free(remainder);
			if (dbt_name)
				free(dbt_name);
			if (dbt)
				fclose(dbt);
			debug_printf(DBGLOG_ERROR, "Failed to insert row: %s\n",
					sqlite3_errmsg(db));
			return rv;
		}
	}

	sqlite3_snprintf(sizeof(sql)-1, sql,
			"INSERT OR REPLACE INTO tablespec ("
			"  tablename,"
			"  dbf_version,"
			"  dbf_reserved1,"
			"  dbf_incomplete,"
			"  dbf_encryption,"
			"  dbf_free_record,"
			"  dbf_multiuser,"
			"  dbf_mdx,"
			"  dbf_language,"
			"  dbf_reserved2,"
			"  dbf_dbc,"
			"  dbf_reserved3,"
			"  dbt_version,"
			"  dbt_reserved1,"
			"  dbt_reserved2"
			") VALUES ("
			"'%s', "	/* Table name */
			"%d, "		/* DBF Version */
			"%d, "		/* DBF Reserved 1 */
			"%d, "		/* DBF Incomplete */
			"%d, "		/* DBF Encryption */
			"%d, "		/* DBF Free record */
			"%Q, "		/* DBF Multi-user */
			"%d, "		/* DBF MDX flag */
			"%d, "		/* DBF Language driver */
			"%d, "		/* DBF Reserved 2 */
			"%Q, "		/* DBF Database Container */
			"%Q, "		/* DBF Reserved 3 */
			"%d, "		/* DBT Version */
			"%Q, "		/* DBT Reserved 1 */
			"%Q"		/* DBT Reserved 2 */
			");",
			table,
			header.version,
			header.reserved1,
			header.incomplete,
			header.encryption,
			header.free_record,
			header.multiuser,
			header.mdx,
			header.language,
			header.reserved2,
			has_dbc ? dbc : NULL,
			remainder,
			dbth.version,
			dbth.reserved1,
			dbth.reserved2);
	rv = sqlite3_prepare(db,
			sql, -1,
			&dbs, NULL);
	debug_printf(DBGLOG_DETAIL, "Updating tablespec for %s\n", table);
	if (rv == SQLITE_OK)
		sqlite3_step(dbs);

	rv = sqlite3_prepare(db,
			"COMMIT;", -1,
			&dbs, NULL);
	if (rv == SQLITE_OK)
		sqlite3_step(dbs);

	fclose(dbf);
	if (remainder)
		free(remainder);
	if (dbt_name)
		free(dbt_name);
	if (dbt)
		fclose(dbt);
	return 0;
}

int main( int argc, char** argv ) {
	const char* prog_name = argv[0];
	argv++; argc--;

	debug_log = stdout;
	while(argc && argv[0][0] == '-') {
		if ((argv[0][0] == '-') &&
				((argv[0][1] == 'q') || (argv[0][1] == 'v'))) {
			const char* a = &argv[0][1];
			while (*a) {
				if (*a == 'q')
					debug_level--;
				else if (*a == 'v')
					debug_level++;
				a++;
			}
		} else if (!strcmp(argv[0], "-n")) {
			debug_level = atoi(argv[1]);
			argc--; argv++;
		} else if (!strcmp(argv[0], "-n")) {
			debug_level = atoi(argv[1]);
			argc--; argv++;
		} else if (!strcmp(argv[0], "-l")) {
			FILE* old_log = debug_log;
			debug_log = fopen(argv[1],"a");
			if (!debug_log) {
				debug_log = old_log;
				debug_printf(DBGLOG_CRITICAL,
						"Unable to open log %s: %s\n",
						argv[1], strerror(errno));
			} else if (old_log) {
				fclose(old_log);
			}
			argc--; argv++;
		}
		argc--; argv++;
	}

	if (argc < 2) {
		debug_printf(DBGLOG_INFO,
				"Usage: %s [-v] [-q] [-l file.log]"
				" [-n log_level] sqlite.db "
				"file.dbf [file2.dbf ...]\n",prog_name);
		return -1;
	}

	sqlite3* db;
	debug_printf(DBGLOG_DETAIL, "Reading sqlite3 database: %s\n", argv[0]);
	int rv = sqlite3_open(argv[0], &db);
	if (rv != SQLITE_OK) {
		if (db) {
			debug_printf(DBGLOG_ERROR, "sqlite tells me: %s\n",
				sqlite3_errmsg(db));
			sqlite3_close(db);
		} else {
			debug_printf(DBGLOG_ERROR, "Failed to set up SQLite database\n");
		}
		return -1;
	}
	argv++; argc--;

	/* Create tables 'fieldspec' and 'tablespec' */
	sqlite3_stmt* dbs;
	rv = sqlite3_prepare(db,
		"CREATE TABLE IF NOT EXISTS tablespec ("
			"tablename		varchar(32), "
			"dbf_version            integer,"
			"dbf_reserved1          integer,"
			"dbf_incomplete         integer,"
			"dbf_encryption         integer,"
			"dbf_free_record        integer,"
			"dbf_multiuser 		blob(8),"
			"dbf_mdx		integer,"
			"dbf_language		integer,"
			"dbf_reserved2		integer,"
			"dbf_dbc                blob(264),"
			"dbf_reserved3          blob(512),"
			"dbt_version            integer,"
			"dbt_reserved1          blob(3),"
			"dbt_reserved2          blob(490),"
			"CONSTRAINT tablespec_pk PRIMARY KEY ("
				"tablename"
			") ON CONFLICT REPLACE);", -1,
			&dbs, NULL);
	debug_printf(DBGLOG_DETAIL, "Creating tablespec table\n");
	if (rv != SQLITE_OK) {
		debug_printf(DBGLOG_CRITICAL,
				"Failed to prepare tablespec creation: %s\n",
				sqlite3_errmsg(db));
		sqlite3_close(db);
		return rv;
	}
	rv = sqlite3_step(dbs);
	if (rv != SQLITE_DONE) {
		debug_printf(DBGLOG_CRITICAL,
				"Failed to execute tablespec creation: %s\n",
				sqlite3_errmsg(db));
		sqlite3_close(db);
		return rv;
	}

	rv = sqlite3_prepare(db,
		"CREATE TABLE IF NOT EXISTS fieldspec ("
			"tablename		varchar(32), "
			"tablecol		integer, "
			"fieldname		varchar(10), "
			"fieldtype		char(1), "
			"fieldaddr		integer, "
			"fieldlen		integer, "
			"fieldcount		integer, "
			"fieldmultiuser1	blob(2), "
			"fieldworkarea		blob(1), "
			"fieldmultiuser2	blob(2), "
			"fieldsetflag		blob(1), "
			"fieldreserved		blob(7), "
			"fieldindex		integer, "
			"CONSTRAINT fieldspec_pk PRIMARY KEY ("
				"tablename, "
				"tablecol, "
				"fieldname "
			") ON CONFLICT REPLACE);", -1,
			&dbs, NULL);
	debug_printf(DBGLOG_DETAIL, "Creating fieldspec table\n");
	if (rv != SQLITE_OK) {
		debug_printf(DBGLOG_ERROR,
				"Failed to prepare fieldspec table creation: %s\n",
				sqlite3_errmsg(db));
		sqlite3_close(db);
		return rv;
	}
	rv = sqlite3_step(dbs);
	if (rv != SQLITE_DONE) {
		debug_printf(DBGLOG_ERROR,
				"Failed to execute fieldspec table creation: %s\n",
				sqlite3_errmsg(db));
		sqlite3_close(db);
		return rv;
	}

	while(argc) {
		debug_printf(DBGLOG_INFO, "Dumping %s to SQLite3\n", argv[0]);
		if (dumpTable(db, argv[0]))
			return -1;
		debug_printf(DBGLOG_INFO, "%s dumped, %d tables to go\n", argv[0], argc-1);
		argv++;
		argc--;
	}
	debug_printf(DBGLOG_INFO, "All complete\n");

	return 0;
}
