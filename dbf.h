/*
 *  DBF to SQLite conversion tool
 *  Copyright (C) 2011  Stuart Longland <me@vk4msl.yi.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef _DBF_H
#define _DBF_H

#include <endian.h>
#include <stdint.h>

/*!
 * On-disk representation of DBF table header
 */
struct dbf_table_header {
	/*! Version or signature field. */
	uint8_t	version;
	/*! Date of last update in little-endian format */
	struct dbf_table_date {
		uint8_t	year;	/*! Year range: 1900 - 2155 */
		uint8_t month;
		uint8_t day;
	}	last_update;
	/*! Number of records, little-endian format */
	uint32_t records;
	/*! Length of header in bytes, little-endian format */
	uint16_t header_size;
	/*! Sum of all field lengths + deletion flag (one byte) */
	uint16_t record_size;
	/*! Should be zero */
	uint16_t reserved1;
	/*! If non-zero, a transaction was started but not completed */
	uint8_t	incomplete;
	/*! If non-zero, data is encrypted (DO NOT MODIFY!) */
	uint8_t encryption;
	/*! Free record thread (reserved for LAN only) */
	uint32_t free_record;
	/*! Reserved for multi-user dBase) */
	uint8_t multiuser[8];
	/*! MDX flag (dBase IV) */
	uint8_t mdx;
	/*! Language driver */
	uint8_t language;
	/*! Reserved bits */
	uint16_t reserved2;
} __attribute__((packed));

/*! Translate dbf_table_header to host representation */
#define dbf_table_header_to_host(p)	{			\
	(p)->records		= le32toh((p)->records);	\
	(p)->header_size	= le16toh((p)->header_size);	\
	(p)->record_size	= le16toh((p)->record_size);	\
	(p)->reserved1	= le16toh((p)->reserved1);		\
	(p)->free_record	= le32toh((p)->free_record);	\
	(p)->reserved2	= le16toh((p)->reserved2);		\
}

/*! Translate dbf_table_header to disk representation */
#define dbf_table_header_to_disk(p)	{			\
	(p)->records		= htole32((p)->records);	\
	(p)->header_size	= htole16((p)->header_size);	\
	(p)->record_size	= htole16((p)->record_size);	\
	(p)->reserved1	= htole16((p)->reserved1);		\
	(p)->free_record	= htole32((p)->free_record);	\
	(p)->reserved2	= htole16((p)->reserved2);		\
}

#define DBF_VER_V3	(0x03)	/*!< Version: DBase III Without DBT */
#define DBF_VER_V3DBT	(0x83)	/*!< Version: DBase III With DBT */
#define DBF_VER_V4	(0x04)	/*!< Version: DBase IV Without DBT */
#define DBF_VER_V4DBT	(0x8b)	/*!< Version: DBase IV With DBT */
#define DBF_VER_VFP	(0x30)	/*!< Version: Visual FoxPro */
#define DBF_VER_VFPAI	(0x31)	/*!< Version: Visual FoxPro (+auto increment) */

/*! Database Container length */
#define DBF_DBC_LEN	(264)

/*! Header terminator */
#define DBF_HEAD_TERM	(0x0d)

/*!
 * On-disk representation of field descriptor
 */
struct dbf_field_desc {
	/*! Field name in ASCII + null */
	uint8_t name[11];
	/*! Field type */
	uint8_t type;
	/*! 
	 * Address in memory (dBase), or offset from beginning of record 
	 * (FoxPro)
	 */
	uint32_t field_addr;
	/*! Field length, up to 255 bytes */
	uint8_t length;
	/*! Decimal count; <= 15 */
	uint8_t count;
	/*! Reserved for multi-user dBase */
	uint8_t multiuser1[2];
	/*! Work area ID: 0x01 for all dBase III files */
	uint8_t work_area;
	/*! Reserved for multi-user dBase */
	uint8_t multiuser2[2];
	/*! Flag for SET FIELDS command */
	uint8_t set_flag;
	/*! Reserved bits */
	uint8_t reserved[7];
	/*! Index field flag */
	uint8_t index;
} __attribute__((packed));

/*! Translate dbf_field_desc to native representation */
#define dbf_field_desc_to_host(p) {		\
	 (p)->field_addr	= le32toh((p)->field_addr);	\
}

/*! Translate dbf_field_desc to disk representation */
#define dbf_field_desc_to_disk(p) {		\
	 (p)->field_addr	= htole32((p)->field_addr);	\
}

/* DBF Field types -- not all defined here */
#define DBF_FIELD_TYPE_CHAR	((uint8_t)('C'))
#define DBF_FIELD_TYPE_NUMBER	((uint8_t)('N'))
#define DBF_FIELD_TYPE_LOGICAL	((uint8_t)('L'))
#define DBF_FIELD_TYPE_DATE	((uint8_t)('D'))
#define DBF_FIELD_TYPE_MEMO	((uint8_t)('M'))

/* DBF Record deletion flag states */
#define DBF_RECORD_NOTDELETED	((uint8_t)(' '))
#define DBF_RECORD_DELETED	((uint8_t)('*'))

/* DBF End-of-file */
#define DBF_EOF			((uint8_t)(0x1a))

/*!
 * DBT Header on-disk format
 */
struct dbt_header {
	/*! Next available block */
	uint32_t	next;
	/*! Block Size (DBase IV) */
	uint32_t	block_size;
	/*! DBF Name (DBase IV) */
	char		dbf_name[8];
	/*! Version number (3 = DBase III, 0 = DBase IV) */
	uint8_t		version;
	/*! Reserved */
	uint8_t		reserved1[3];
	/*! Block Length (DBase IV) */
	uint16_t	block_length;
	/*! Reserved */
	uint8_t		reserved2[490];
} __attribute__((packed));

/*! Translate dbt_header to native representation */
#define dbt_header_to_host(p)	{				\
	(p)->next		= le32toh((p)->next);		\
	(p)->block_size		= le32toh((p)->block_size);	\
	(p)->block_length	= le16toh((p)->block_length);	\
}

/*! Translate dbf_field_desc to disk representation */
#define dbt_header_to_disk(p)	{				\
	(p)->next		= htole32((p)->next);		\
	(p)->block_size		= htole32((p)->block_size);	\
	(p)->block_length	= htole16((p)->block_length);	\
}

/*! DBT End of block */
#define DBT_EOB			((uint8_t)(0x1a))

/*! DBT Block Size */
#define DBT_BS			512

/*! DBT: DBase version III */
#define DBT_VER3		0x03

/*! DBT: DBase version IV */
#define DBT_VER4		0x00

/*! DBT Block-end first character */
#define DBT_BE0			((uint8_t)(0x0d))
/*! DBT Block-end second character */
#define DBT_BE1 		((uint8_t)(0x0a))

/*! DBT Line-end first character */
#define DBT_LE0			((uint8_t)(0x8d))
/*! DBT Line-end second character */
#define DBT_LE1 		((uint8_t)(0x0a))

/*!
 * DBT Used Block; for DBase IV files
 */
struct dbt_usedblock_header {
	/*! Magic bytes */
	uint32_t	magic;
	/*! Length of field */
	uint32_t	length;
} __attribute__((packed));

/*! Translate dbt_usedblock_header to native representation */
#define dbt_usedblock_header_to_host(p)	{		\
	(p)->magic		= le32toh((p)->magic);	\
	(p)->length		= le32toh((p)->length);	\
}

/*! Translate dbf_usedblock_header to disk representation */
#define dbt_usedblock_header_to_disk(p)	{	\
	(p)->magic	= htole32((p)->magic);	\
	(p)->length	= htole32((p)->length);	\
}

/*! Magic byte sequence to look for */
#define DBT_USED_MAGIC		(0x0008fffful)

/*!
 * DBT Unused Block; for DBase IV files
 */
struct dbt_unusedblock_header {
	/*! Next free block */
	uint32_t	next_free;
	/*! Next in-use block */
	uint32_t	next_used;
} __attribute__((packed));

/*! Translate dbt_unusedblock_header to native representation */
#define dbt_unusedblock_header_to_host(p) {			\
	(p)->next_free		= le32toh((p)->next_free);	\
	(p)->next_used		= le32toh((p)->next_used);	\
}

/*! Translate dbf_usedblock_header to disk representation */
#define dbt_unusedblock_header_to_disk(p) {		\
	(p)->next_free	= htole32((p)->next_free);	\
	(p)->next_used	= htole32((p)->next_used);	\
}

/*!
 * Block union; use this to determine what sort of block you have
 */
union dbt_block_header {
	/*! DBase IV Used Block */
	struct dbt_usedblock_header	v4used;
	/*! DBase IV Unused Block */
	struct dbt_unusedblock_header	v4unused;
};

/*! Translate dbt_block_header to native representation */
#define dbt_block_header_to_host(p) \
	dbt_unusedblock_header_to_host(&((p)->v4unused))

/*! Translate dbf_block_header to disk representation */
#define dbt_block_header_to_disk(p) \
	dbt_unusedblock_header_to_disk(&((p)->v4unused))

#endif
